
package relacionpdo;

import informacion.Clinica;
import informacion.Dolencia;
import java.util.Scanner;
import personas.Fisioterapeuta;
import personas.Paciente;


public class RelacionPDO {
    
    /**
     * Funcion main que ejecuta los ejercicios de la relacion de PDO
     * @param args 
     */
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        
        // Ejercicio 2
        
        System.out.println("Escriba el dia, el mes y el año para la fecha 1: (Pulse intro cada vez que haya terminado de insertar dicho dato)");
        Fecha fecha1=new Fecha(s.nextByte(), s.nextByte(), s.nextShort());
        if (fecha1.getDia()>=31) {
            fecha1.setDia(fecha1.getDia()); // Esta funcion ajusta el dia de cada mes en caso de que el dia introducido sea un numero mayor
        }
        
        System.out.println("Escriba el dia, el mes y el año para la fecha 2: (Pulse intro cada vez que haya terminado de insertar dicho dato)");
        Fecha fecha2=new Fecha(s.nextByte(), s.nextByte(), s.nextShort());
        if (fecha2.getDia()>=31) {
            fecha1.setDia(fecha1.getDia()); // Esta funcion ajusta el dia de cada mes en caso de que el dia introducido sea un numero mayor
        }
        
        System.out.println("Fecha 1: "+fecha1.getDia()+"/"+fecha1.getMes()+"/"+fecha1.getAño());
        System.out.println("Fecha 2: "+fecha2.getDia()+"/"+fecha2.getMes()+"/"+fecha2.getAño());
        if (Fecha.mayorQue(fecha1, fecha2)==true) {
            System.out.println("La fecha mas grande es Fecha 1");
        } else if (Fecha.mayorQue(fecha1, fecha2)==false&&(fecha1.getAño()==fecha2.getAño()&&fecha1.getMes()==fecha2.getMes()&&fecha1.getDia()==fecha2.getDia())) {
            System.out.println("Has introducido 2 fechas iguales");
        } else {
            System.out.println("La fecha más grande es Fecha 2");
        }
        
        // Fin Ejercicio 2
        
        
        // Ejercicio 4
        
        // Crear 9 dolencias (Mínimo 2 para cada tipo)
        Dolencia dolencia1=new Dolencia(1, "Codos y Rodillas");
        Dolencia dolencia2=new Dolencia(2, "Espalda y Cuello");
        Dolencia dolencia3=new Dolencia(3, "Manos y Pies");
        Dolencia dolencia4=new Dolencia(4, "Espalda y Cuello");
        Dolencia dolencia5=new Dolencia(5, "Codos y Rodillas");
        Dolencia dolencia6=new Dolencia(6, "Manos y Pies");
        Dolencia dolencia7=new Dolencia(7, "Manos y Pies");
        Dolencia dolencia8=new Dolencia(8, "Codos y Rodillas");
        Dolencia dolencia9=new Dolencia(9, "Espalda y Cuello");
        
        // Crear 9 pacientes, cada uno con su dolencia
        Paciente pacienteTomas=new Paciente("Tomás", "783748336", "73639475A", dolencia9);
        Paciente pacienteLeonardo=new Paciente("Leonardo", "123456789", "98765432B", dolencia4);
        Paciente pacienteJaime=new Paciente("Jaime", "626785303", "12345678B", dolencia8);
        Paciente pacienteLuis=new Paciente("Luis", "857473253", "67356364F", dolencia7);
        Paciente pacientePepe=new Paciente("Pepe", "640283748", "42785905G", dolencia5);
        Paciente pacienteRestia=new Paciente("Restia", "734636451", "98654946G", dolencia2);
        Paciente pacienteTohka=new Paciente("Tohka", "754895312", "12312345H", dolencia1);
        Paciente pacienteToto=new Paciente("Toto", "784568465" ,"98798765Q" ,dolencia6);
        Paciente pacientePeter=new Paciente("Peter", "457876996" ,"84959689U" ,dolencia3);
        
        // Fin Ejercicio 4
        
        
        // Ejercicio 5
        
        // Crear 3 fisioterapeutas, cada uno con una especialidad distinta
        Fisioterapeuta fisio1=new Fisioterapeuta("Fisio 1", "748787355", "90597484S", "Av. de Concha Espina, 1", "Espalda y Cuello");
        Fisioterapeuta fisio2=new Fisioterapeuta("Fisio 2", "378298573", "29894095F", "Calle Miguel de Merida Nicolich, 2", "Codos y Rodillas");
        Fisioterapeuta fisio3=new Fisioterapeuta("Fisio 3", "905894745", "39048853M", "Explanada de la Estación", "Manos y Pies");
        
        // Crear una clinica
        Fisioterapeuta[] fisioCenec={fisio1, fisio2, fisio3};
        Clinica clinicaCenec=new Clinica("Clinica CENEC", "952774303", "E93783588", "Alameda Principal, 16, 29005 Málaga", fisioCenec);
        
        // Fin Ejercicio 5
    }
    
}
