
package relacionpdo;

/**
 * Clase que modela una fecha
 * @author Darash Chablani
 */
public class Fecha {
    private byte dia; // Dia de la fecha
    private byte mes; // Mes de la fecha
    private short año; // Año de la fecha
    
    /**
     * Constructor de la clase Fecha
     * @param d Dia de la fecha
     * @param m Mes de la fecha
     * @param a Año de la fecha
     */
    public Fecha (byte d, byte m, short a) {
        this.dia=ajusteRealistaDia(d, m);
        this.mes=ajusteRealistaMes(m);
        this.año=ajusteRealistaAño(a);
    }    
    
    /**
     * Getter de la variable dia
     * @return Dia de una fecha
     */
    public byte getDia() {
        return dia;
    }
    
    /**
     * Setter de la variable dia
     * @param dia Establece el dia de una fecha
     */
    public void setDia(byte dia) {
        this.dia = ajusteRealistaDia(dia, getMes());
    }
    
    /**
     * Getter de la variable mes
     * @return Mes de la fecha
     */
    public byte getMes() {
        return mes;
    }
    
    /**
     * Setter de la variable mes
     * @param mes Establece el mes de una fecha
     */
    public void setMes(byte mes) {
        this.mes = ajusteRealistaMes(mes);
    }
    
    /**
     * Getter de la variable año
     * @return Año de una fecha
     */
    public short getAño() {
        return año;
    }
    
    /**
     * Setter de la variable año
     * @param año Establece el año de una fecha
     */
    public void setAño(short año) {
        this.año = ajusteRealistaAño(año);
    }
    
    /**
     * Funcion que ajusta automaticamente el año en caso de introducir un año que no sea apto
     * @param a Año introducido
     * @return Año ajustado
     */
    public short ajusteRealistaAño(short a) {
        if (a>=0&&a<=99) {
            if (a<=49) {
                return (short) (2000+a);
            } else if (a>=50) {
                return (short) (1900+a);
            }
        } else if (a<0) {
            return 0;
        } else if (a>2100) {
            return 2100;
        } else {
            return a;
        }
        return a;
    }
    
    /**
     * Funcion que ajusta automaticamente el mes en caso de introducir un mes que no sea apto
     * @param m Mes introducido
     * @return Mes ajustado
     */
    public byte ajusteRealistaMes(byte m) {
        if(m>=1&&m<=12) {
            return m;
        } else if (m<1) {
            return 1;
        } else if (m>12) {
            return 12;
        }
        
        return m;
    }
    
    /**
     * Funcion que ajusta automaticamente el dia en caso de introducir un dia que no sea apto
     * En caso de que el dia introducido no sea apto, el dia maximo dependera del mes en que esté
     * @param d Dia introducido
     * @param m Mes introducido
     * @return Dia ajustado
     */
    public byte ajusteRealistaDia(byte d, byte m) {
        if (d>=1&&d<31) {
            return d;
        } else if (d<1) {
            return 1;
        } else if (d>=31) {
            if (m<1||m>12) {
                m=ajusteRealistaMes(m);
            }
            switch(m) {
                case 1:
                    d=31;
                    break;
                case 3:
                    d=31;
                    break;
                case 5:
                    d=31;
                    break;
                case 7:
                    d=31;
                    break;
                case 8:
                    d=31;
                    break;
                case 10:
                    d=31;
                    break;
                case 12:
                    d=31;
                    break;
                case 2:
                    d=30;
                    break;
                case 4:
                    d=30;
                    break;
                case 6:
                    d=30;
                    break;
                case 9:
                    d=30;
                    break;
                case 11:
                    d=30;
                    break;
            }
        }
        
        return d;
    }
    
    /**
     * Funcion que recibe como argumento 2 fechas y averigua cual de ellas es mayor
     * @param f1 Primera fecha recibido por argumento
     * @param f2 Segunda fecha recibida por argumento
     * @return true si la primera fecha es mayor que la segunda. false si las 2 fechas son iguales o que la segunda fecha es mayor que la primera
     */
    public static boolean mayorQue(Fecha f1, Fecha f2) {
        if (f1.getAño()>f2.getAño()) {
            return true;
        } else if (f1.getAño()==f2.getAño()) {
            if (f1.getMes()>f2.getMes()) {
                return true;
            } else if (f1.getMes()==f2.getMes()) {
                if (f1.getDia()>f2.getDia()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
