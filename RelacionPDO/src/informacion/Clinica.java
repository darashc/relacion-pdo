
package informacion;

import personas.Fisioterapeuta;
import personas.Paciente;

/**
 * Clase que modela una clínica
 * @author Darash Chablani
 */
public class Clinica extends Elemento {
    private String direccion; // Direccion de la clinica
    private Fisioterapeuta[] fisioterapeutas; // Fisioterapeutas contratados en la clinica
    private Paciente[] paciente; // Pacientes ingresados en la clinica
    
    /**
     * Constructor de la clase Clinica
     * @param n Nombre de la clinica
     * @param t Nº de telefono de la clinica
     * @param id Nº identificativo de la clinica (CIF)
     * @param dir Direccion de la clinica
     * @param f Fisioterapeutas contratados en la clinica
     */
    public Clinica(String n, String t, String id, String dir, Fisioterapeuta[] f) {
        super(n, t, id);
        this.direccion=dir;
        this.fisioterapeutas=f;
    }
    
    /**
     * Getter de la variable dirección
     * @return Dirección de la clinica
     */
    public String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter de la variable direccion
     * @param direccion Establece la direccion de la clinica
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter de la variable fisioterapeutas
     * @return Fisioterapeutas en la clinica
     */
    public Fisioterapeuta[] getFisioterapeutas() {
        return fisioterapeutas;
    }
     /**
      * Setter de la variable fisioterapeutas
      * @param fisioterapeutas Establece las fisioterapeutas en el centro
      */
    public void setFisioterapeutas(Fisioterapeuta[] fisioterapeutas) {
        this.fisioterapeutas = fisioterapeutas;
    }
    
    /**
     * Getter de la variable paciente
     * @return Paciente ingresado en la clinica
     */
    public Paciente[] getPaciente() {
        return paciente;
    }
    
    /**
     * Setter de la variable paciente
     * @param paciente Establece los pacientes en la clinica
     */
    public void setPaciente(Paciente[] paciente) {
        this.paciente = paciente;
    }
}
