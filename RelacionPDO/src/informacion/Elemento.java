
package informacion;

/**
 * Clase que contiene los elementos (repetidos) de una clínica
 * @author Darash Chablani
 */
public class Elemento {
    private String nombre; // Nombre del elemento
    private String nTelefono; // Nº de teléfono del elemento
    private String nIdentificacion; // Nº de identificación del elemento (DNI/NIF)
    
    public Elemento (String n, String t, String id) {
        this.nombre=n;
        this.nTelefono=t;
        this.nIdentificacion=id;
    }
    
    /**
     * Getter de la variable nombre
     * @return Nombre de dicho elemento
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * Setter de la variable nombre
     * @param nombre Establece el nombre de dicho elemento
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter de la variable nTelefono
     * @return Nº de telefono de dicho elemento
     */
    public String getnTelefono() {
        return nTelefono;
    }
     /**
      * Setter de la variable nTelefono
      * @param nTelefono Establece el nº de telefono de dicho elemento
      */
    public void setnTelefono(String nTelefono) {
        this.nTelefono = nTelefono;
    }
    
    /**
     * Getter de la variable nIdentificacion
     * @return El Nº de identificacion (DNI o CIF) de dicho elemento
     */
    public String getnIdentificacion() {
        return nIdentificacion;
    }
    
    /**
     * Setter de la variable nIdentificacion
     * @param nIdentificación Establece el Nº de identificacion (DNI o CIF) de dicho elemento
     */
    public void setnIdentificacion(String nIdentificación) {
        this.nIdentificacion = nIdentificación;
    }
    
    
}
