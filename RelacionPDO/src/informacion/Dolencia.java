
package informacion;

/**
 * Clase que modela una dolencia
 * @author Darash Chablani
 */
public class Dolencia {
    private int valorGravedad; // Valor de la gravedad de la dolencia (Entre 0 (Menos grave) y 10 (Extremadamente Grave))
    private String tipo; // Tipo de dolencia ("Espalda y cuello", "Codos y Rodillas", "Manos y Pies")
    
    public Dolencia (int g, String t) {
        this.valorGravedad=g;
        this.tipo=t;
    }

    /**
     * Getter de la variable valorGravedad
     * @return El valor de la gravedad de la dolencia (Entre 0 (Menos grave) y 10 (Extremadamente Grave))
     */
    public int getValorGravedad() {
        return valorGravedad;
    }
    
    /**
     * Setter de la variable valorGravedad
     * @param valorGravedad Establece el valor de la gravedad de la dolencia (Entre 0 (Menos grave) y 10 (Extremadamente Grave))
     */
    public void setValorGravedad(int valorGravedad) {
        this.valorGravedad = valorGravedad;
    }
    
    /**
     * Getter de la variable tipo
     * @return Tipo de dolencia ("Espalda y cuello", "Codos y Rodillas", "Manos y Pies")
     */
    public String getTipo() {
        return tipo;
    }
    
    /**
     * Setter de la variable tipo
     * @param tipo Eestablece el tipo de dolencia ("Espalda y cuello", "Codos y Rodillas", "Manos y Pies")
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
}
