
package personas;

import informacion.Dolencia;
import informacion.Elemento;
import relacionpdo.Fecha;



/**
 * Clase que modela un paciente
 * @author Darash Chablani
 */
public class Paciente extends Elemento {
    private Dolencia dolencia; // Dolencia del paciente
    private Fecha primeraCita; // Fecha de la primera cita del paciente
    private Fecha ultimaCita; // Fecha de la ultima cita del paciente
    private int nDiasEntreCitas; // Numero de dias entre sus citas
    private int nCitasRestantes; // Numero de citas restantes para el fin del tratamiento del paciente
    private Fecha proximaCita; // Fecha de la proxima cita del paciente
    
    /**
     * Constructor de la clase Paciente
     * @param n Nombre del paciente
     * @param t Nº de telefono del paciente
     * @param id Nº identificativo del paciente (DNI)
     * @param d Direccion de vivienda del paciente
     */
    public Paciente (String n, String t, String id, Dolencia d) {
        super(n, t, id);
        this.dolencia=d;
    }
    
    /**
     * Getter de la variable dolencia del paciente
     * @return La dolencia del paciente (Tipo de dolencia y su gravedad)
     */
    public Dolencia getDolencia() {
        return dolencia;
    }
    
    /**
     * Setter de la variable dolencia del paciente
     * @param dolencia Establece la dolencia del paciente (Tipo de dolencia y su gravedad)
     */
    public void setDolencia(Dolencia dolencia) {
        this.dolencia = dolencia;
    }
    
    /**
     * Getter de la variable primeraCita
     * @return Fecha de la primera cita del paciente
     */
    public Fecha getPrimeraCita() {
        return primeraCita;
    }
    
    /**
     * Setter de la variable primeraCCita
     * @param primeraCita Establace la fecha de la primera cita del paciente
     */
    public void setPrimeraCita(Fecha primeraCita) {
        this.primeraCita = primeraCita;
    }
    
    /**
     * Getter de la variable ultimaCita
     * @return Fecha de la ultima cita del paciente
     */
    public Fecha getUltimaCita() {
        return ultimaCita;
    }
    
    /**
     * Setter de la variable ultimaCita
     * @param ultimaCita Establece la fecha de la ultima cita el paciente
     */
    public void setUltimaCita(Fecha ultimaCita) {
        this.ultimaCita = ultimaCita;
    }
    
    /**
     * Getter de la variable nDiasEntreCitas
     * @return Numero de dias entre las citas del paciente
     */
    public int getnDiasEntreCitas() {
        return nDiasEntreCitas;
    }
    
    /**
     * Setter de la variable nDiasEntreCitas
     * @param nDiasEntreCitas Establece el numero de dias entre las citas del paciente
     */
    public void setnDiasEntreCitas(int nDiasEntreCitas) {
        this.nDiasEntreCitas = nDiasEntreCitas;
    }
    
    /**
     * Getter de la variable nCitasRestantes
     * @return Numero de citas restantes
     */
    public int getnCitasRestantes() {
        return nCitasRestantes;
    }
    
    /**
     * Setter de la variable nCitasRestantes
     * @param nCitasRestantes Establece el numero de citas restantes
     */
    public void setnCitasRestantes(int nCitasRestantes) {
        this.nCitasRestantes = nCitasRestantes;
    }
    
    /**
     * Getter de la variable proximaCita
     * @return Fecha de la proxima cita del paciente
     */
    public Fecha getProximaCita() {
        return proximaCita;
    }
    
    /**
     * Setter de la variable proximaCita
     * @param proximaCita Estabñece la fecha de la proxima cita del paciente
     */
    public void setProximaCita(Fecha proximaCita) {
        this.proximaCita = proximaCita;
    }
    
    
    
}
