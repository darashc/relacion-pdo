
package personas;

import informacion.Elemento;

/**
 * Clase que modela un Fisioterapeuta
 * @author Darash Chablani
 */
public class Fisioterapeuta extends Elemento {
    private String direccion; // Dieccion de vivienda del fisioterapeuta
    private String especialidad; // Especialidad del fisio. ("Espalda y cuello", "Codos y Rodillas", "Manos y Pies")
    private Paciente[] pacientesAsignados;
    
    /**
     * Constructor de la clase Fisioterapeuta
     * @param n Nombre del fisioterapeuta
     * @param t Nº de telefono del fisioterapeuta
     * @param id Nº de identificacion del fisioterapeuta (DNI)
     * @param dir Direccion de vivienda del fisioterapeuta
     * @param esp Especialidad del fisioterapeuta ("Espalda y cuello", "Codos y Rodillas", "Manos y Pies")
     */
    public Fisioterapeuta(String n, String t, String id, String dir, String esp) {
        super(n, t, id);
        this.direccion=dir;
        this.especialidad=esp;
    }
    
    /**
     * Getter de la variable dirreccion
     * @return Direccion del fisio
     */
    public String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter de la variable direccion
     * @param direccion Establece la direccion del fisio
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter de la variable especialidad
     * @return Especialidad del fisio ("Espalda y cuello", "Codos y Rodillas", "Manos y Pies")
     */
    public String getEspecialidad() {
        return especialidad;
    }
    
    /**
     * Setter de la variable especialidad
     * @param especialidad Establece la especialidad del fisio ("Espalda y cuello", "Codos y Rodillas", "Manos y Pies")
     */
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    
    /**
     * Setter de la variable PacientesAsignados
     * @return Pacientes Asignados del fisio
     */
    public Paciente[] getPacientesAsignados() {
        return pacientesAsignados;
    }
    
    /**
     * Setter de la variable PacientesAsignados
     * @param pacientesAsignados Establece los pacientes asignados del fisio
     */
    public void setPacientesAsignados(Paciente[] pacientesAsignados) {
        this.pacientesAsignados = pacientesAsignados;
    }
    
}
